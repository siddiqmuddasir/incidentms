package Utilities;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

public class ExcelFileUtil {
	
	Workbook wb;
	//It Will load all the excel sheet
	public ExcelFileUtil() throws Exception
	{
		FileInputStream fis = new FileInputStream("./TestInputs/InputSheet.xlsx");
		
		 wb = WorkbookFactory.create(fis);
	}
	public int rowCount(String sheetname)
	{
		return wb.getSheet(sheetname).getLastRowNum();
	}
	public int colCount(String sheetname, int rowNo)
	{
		return wb.getSheet(sheetname).getRow(rowNo).getLastCellNum();
	}
	
	//for retrieving the excel data
	
	public String getData(String sheetname,int row, int column)
	{
		String data= "";
		
		if (wb.getSheet(sheetname).getRow(row).getCell(column).getCellType() == Cell.CELL_TYPE_NUMERIC)
		{
			int celldata = (int) wb.getSheet(sheetname).getRow(row).getCell(column).getNumericCellValue();
			
			data = String.valueOf(celldata);
		}
		
		else 
		{
			data = wb.getSheet(sheetname).getRow(row).getCell(column).getStringCellValue();
		}
		
		return data;
	}
	
	//For satus
	
	public void setData(String sheetname,int row, int column, String data) throws Exception
	{
		Sheet sh = wb.getSheet(sheetname);
		Row rowNum = sh.getRow(row);
		Cell cell = rowNum.createCell(column);
		cell.setCellValue(data);
		
		if(data.equalsIgnoreCase("Pass"))
		{
			CellStyle style = wb.createCellStyle();
			Font font = wb.createFont();
			//Apply color to the text
			font.setColor(IndexedColors.GREEN.getIndex());
			//Apply Bold To the text
			font.setBoldweight(Font.BOLDWEIGHT_BOLD);
			style.setFont(font);
			rowNum.getCell(column).setCellStyle(style);
		}
		else
			if(data.equalsIgnoreCase("Fail"))
			{
				CellStyle style = wb.createCellStyle();
				Font font = wb.createFont();
				//Apply color to the text
				font.setColor(IndexedColors.RED.getIndex());
				//Apply Bold To the text
				font.setBoldweight(Font.BOLDWEIGHT_BOLD);
				style.setFont(font);
				rowNum.getCell(column).setCellStyle(style);
			}
			else
				if(data.equalsIgnoreCase("Not Executed"))
				{
					CellStyle style = wb.createCellStyle();
					Font font = wb.createFont();
					//Apply color to the text
					font.setColor(IndexedColors.BLUE.getIndex());
					//Apply Bold To the text
					font.setBoldweight(Font.BOLDWEIGHT_BOLD);
					style.setFont(font);
					rowNum.getCell(column).setCellStyle(style);
				}
		FileOutputStream fos = new FileOutputStream("./TestOutputs/OutputSheet.xlsx");
		wb.write(fos);
		fos.close();
	}
}
