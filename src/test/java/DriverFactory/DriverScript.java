package DriverFactory;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import CommonFunctionLibrary.FunctionLibrary;
import Utilities.ExcelFileUtil;

public class DriverScript 
{
	WebDriver driver;
	ExtentReports report;
	ExtentTest logger;
	
	public void startTest() throws Throwable
	{
		ExcelFileUtil excel = new ExcelFileUtil();
		for (int i=1;i<=excel.rowCount("MasterTestCases");i++)
		{
			String ModuleStatus = "";
			if(excel.getData("MasterTestCases", i, 2).equalsIgnoreCase("Y"))
			{
				String TCModule = excel.getData("MasterTestCases", i, 1);
				
				//Generating Extent Reports
				report = new ExtentReports("H:/Sumfive Automation/Lean_Sticky_Notes/Reports"+TCModule+".html"+"_"+FunctionLibrary.generateDate());
				logger = report.startTest(TCModule);	
				
				int rowcount= excel.rowCount(TCModule);
				
				for(int j=1;j<=rowcount;j++)
				{
					String Description = excel.getData(TCModule, j, 0);
					String Object_Type = excel.getData(TCModule, j, 1);
					String Locator_Type = excel.getData(TCModule, j, 2);
					String Locator_Value = excel.getData(TCModule, j, 3);
					String Test_Data = excel.getData(TCModule, j, 4);
					try
					{
					if(Object_Type.equalsIgnoreCase("startBrowser"))
					{
						driver = FunctionLibrary.startBrowser(driver);
						logger.log(LogStatus.INFO, Description);
					}
					if(Object_Type.equalsIgnoreCase("OpenApp"))
					{
						FunctionLibrary.OpenApp(driver);
						logger.log(LogStatus.INFO, Description);
					}
					if(Object_Type.equalsIgnoreCase("clickaction"))
					{
						FunctionLibrary.clickaction(driver, Locator_Type, Locator_Value);
						logger.log(LogStatus.INFO, Description);
					}
					if(Object_Type.equalsIgnoreCase("typeaction"))
					{
						FunctionLibrary.typeaction(driver, Locator_Type, Locator_Value,Test_Data );
						logger.log(LogStatus.INFO, Description);
					}
					if(Object_Type.equalsIgnoreCase("selectdropdown"))
					{
						FunctionLibrary.selectdropdown(driver, Locator_Type, Locator_Value, Test_Data );
						logger.log(LogStatus.INFO, Description);
					}
					if(Object_Type.equalsIgnoreCase("focus"))
					{
						FunctionLibrary.focus(driver);
						logger.log(LogStatus.INFO, Description);
					}
					if(Object_Type.equalsIgnoreCase("DropdownAction"))
					{
						FunctionLibrary.DropdownAction(driver, Locator_Type, Locator_Value, Test_Data );
						logger.log(LogStatus.INFO, Description);
					}
					if(Object_Type.equalsIgnoreCase("switchtoframe"))
					{
						FunctionLibrary.switchtoframe(driver, Locator_Type, Locator_Value,Test_Data );
						logger.log(LogStatus.INFO, Description);
					}
					if(Object_Type.equalsIgnoreCase("waitforelement"))
					{
						FunctionLibrary.waitforelement(driver, Locator_Type, Locator_Value,Test_Data );
						logger.log(LogStatus.INFO, Description);
					}
					if(Object_Type.equalsIgnoreCase("closebrowser"))
					{
						FunctionLibrary.closebrowser(driver);
						logger.log(LogStatus.INFO, Description);
					}
					if(Object_Type.equalsIgnoreCase("AddClick"))
					{
						FunctionLibrary.AddClick(driver, Locator_Type, Locator_Value,Test_Data );
						logger.log(LogStatus.INFO, Description);
					}
					if(Object_Type.equalsIgnoreCase("Ok"))
					{
						FunctionLibrary.Ok(driver, Locator_Type, Locator_Value );
						logger.log(LogStatus.INFO, Description);
					}
					if(Object_Type.equalsIgnoreCase("Ok1"))
					{
						FunctionLibrary.Ok1(driver, Locator_Value );
						logger.log(LogStatus.INFO, Description);
					}
					
					if(Object_Type.equalsIgnoreCase("UserValidation"))
				    {
					FunctionLibrary.UserValidation(driver, Test_Data);
					logger.log(LogStatus.INFO, Description);
				    }
					
					if(Object_Type.equalsIgnoreCase("MultiselectClick"))
					{
						FunctionLibrary.MultiselectClick(driver);
						logger.log(LogStatus.INFO, Description);
					}
					
					if(Object_Type.equalsIgnoreCase("MultiselectMembers"))
					{
						FunctionLibrary.MultiselectMembers(driver, Locator_Type, Locator_Value,Test_Data);
						logger.log(LogStatus.INFO, Description);
					}
//					if(Object_Type.equalsIgnoreCase("Site"))
//					{
//						FunctionLibrary.Site(driver,Test_Data);
//					}
					
					if(Object_Type.equalsIgnoreCase("threadsleep"))
					{
						FunctionLibrary.threadsleep(driver );
						logger.log(LogStatus.INFO, Description);
					}
					
					excel.setData(TCModule, j, 5, "Pass");
					ModuleStatus = "true";
					logger.log(LogStatus.PASS, Description+" Pass");
					
				}
					catch (Exception e)
					{
						excel.setData(TCModule, j, 5, "Fail");
						ModuleStatus = "false";
						logger.log(LogStatus.FAIL, Description+"  Fail");
						
						//Generating Sceenshots
						File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
						FileUtils.copyFile(scrFile, new File("./Screenshots/"+Description+"_"+FunctionLibrary.generateDate()+".jpg"));	
						break;
						
					}
					catch (AssertionError a)
					{
					excel.setData(TCModule, j, 5, "Not Excecuted");
					ModuleStatus= "false";
					logger.log(LogStatus.FAIL, Description+"  Fail");
					
					//Generating Sceenshots
					File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
					FileUtils.copyFile(scrFile, new File("./Screenshots/"+Description+"_"+FunctionLibrary.generateDate()+".jpg"));	
					break;
					}
					
				}
				if (ModuleStatus.equalsIgnoreCase("true"))
				{
						excel.setData("MasterTestCases", i, 3, "Pass");
				}
				else
				{
					excel.setData("MasterTestCases", i, 3, "Fail");
				}
				
			}
			else
			{
				excel.setData("MasterTestCases", i, 3, "Not Executed");
			}
			report.endTest(logger);
			report.flush();
		}
		
	}
	}


