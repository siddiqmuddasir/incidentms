package CommonFunctionLibrary;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import Utilities.PropertyFileUtil;

public class FunctionLibrary 
{
	//StartBrowser
	public static WebDriver startBrowser(WebDriver driver) throws Throwable, Exception
	{
		if(PropertyFileUtil.getValueForKey("Browser").equalsIgnoreCase("Chrome"))
		{
			System.setProperty("webdriver.chrome.driver", "./CommonJarFiles/chromedriver.exe");
			 driver= new ChromeDriver();
			
		}
		else
			if(PropertyFileUtil.getValueForKey("Browser").equalsIgnoreCase("Firefox")){
				driver = new FirefoxDriver();
			}
		
		return driver;
	}
	
	//Open Application
	public static void OpenApp(WebDriver driver) throws Throwable, Exception
	{
		driver.manage().window().maximize();
		driver.get(PropertyFileUtil.getValueForKey("Url"));
	}
	
	//ClickAction Method
	public static void clickaction(WebDriver driver, String LocatorType, String LocatorValue)
	{
		if(LocatorType.equalsIgnoreCase("id"))
		{
			driver.findElement(By.id(LocatorValue)).click();
		}
		else
			if(LocatorType.equalsIgnoreCase("name"))
			{
				driver.findElement(By.name(LocatorValue)).click();
			}
		else
			if(LocatorType.equalsIgnoreCase("xpath"))
			{
				driver.findElement(By.xpath(LocatorValue)).click();
			}
	}
	
	//Giving data into the textboxes type action
	public static void typeaction(WebDriver driver, String LocatorType, String LocatorValue, String Data)
	{
		if(LocatorType.equalsIgnoreCase("id"))
		{
			driver.findElement(By.id(LocatorValue)).clear();
			driver.findElement(By.id(LocatorValue)).sendKeys(Data);
		}
		else
			if(LocatorType.equalsIgnoreCase("name"))
			{
				driver.findElement(By.name(LocatorValue)).clear();
				driver.findElement(By.name(LocatorValue)).sendKeys(Data);
			}
		else
			if(LocatorType.equalsIgnoreCase("xpath"))
			{
				driver.findElement(By.xpath(LocatorValue)).clear();
				driver.findElement(By.xpath(LocatorValue)).sendKeys(Data);
			}
	}
	
	public static void titleValidation(WebDriver driver, String exp_title)
	{
		String act_title =driver.getTitle();
		Assert.assertEquals(act_title, exp_title);
		
	}
	
	public static String generateDate()
	{
		Date date =new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy_mm_dd_ss");
		return sdf.format(date);
	}
	
	public static void DropdownAction(WebDriver driver, String LocatorType, String LocatorValue, String Data) throws Throwable
	{
		if(LocatorType.equalsIgnoreCase("id"))
		{
			WebElement wb = driver.findElement(By.id(LocatorValue));
			Actions mouse = new Actions(driver);
			  mouse.moveToElement(wb).click().perform();
			  Select sel = new Select(wb);
			  sel.selectByVisibleText(Data);
			  wb.sendKeys(Keys.TAB);
			  
			  
		}
		else
			if(LocatorType.equalsIgnoreCase("name"))
			{
				WebElement wb = driver.findElement(By.name(LocatorValue));
				Actions mouse = new Actions(driver);
				  mouse.moveToElement(wb).click().perform();
				  Select sel = new Select(wb);
				  sel.selectByVisibleText(Data);
				  wb.sendKeys(Keys.TAB);
				 
				  
			}
		else
			if(LocatorType.equalsIgnoreCase("xpath"))
			{
				WebElement wb = driver.findElement(By.xpath(LocatorValue));
				Actions mouse = new Actions(driver);
				  mouse.moveToElement(wb).click().perform();
				  Select sel = new Select(wb);
				  sel.selectByVisibleText(Data);
				  wb.sendKeys(Keys.TAB);
				 
				  
			}
	}
	public static void captureData(WebDriver driver, String locatorType, String locatorValue) throws Throwable
	{

		Thread.sleep(3000);		

		String data = "";
		
		if(locatorType.equalsIgnoreCase("id"))
		{
			data = driver.findElement(By.id(locatorValue)).getAttribute("value");
		}
		else
			if(locatorType.equalsIgnoreCase("xpath"))
			{
				data = driver.findElement(By.xpath(locatorValue)).getAttribute("value");
			}
		
		FileWriter fw = new FileWriter("./CapturedData/Data.txt");
		
		BufferedWriter bw = new BufferedWriter(fw);
		
		bw.write(data);
		
		bw.flush();
		
		bw.close();		
	}
	public static void datePicker(WebDriver driver, String locatorValue, String date)
	{
		((JavascriptExecutor)driver).executeScript("document.getElementById('"+locatorValue+"').value='"+date+"'");
	}
	
	public static void Ok(WebDriver driver, String LocatorType, String LocatorValue) throws Exception
	{
		if(LocatorType.equalsIgnoreCase("id"))
		{
			Thread.sleep(3000);
			driver.findElement(By.id(LocatorValue)).click();
		}
		else
			if(LocatorType.equalsIgnoreCase("name"))
		{
			Thread.sleep(3000);
			driver.findElement(By.name(LocatorValue)).click();
		}
			else
				if(LocatorType.equalsIgnoreCase("xpath"))
		{
			Thread.sleep(3000);
			driver.findElement(By.xpath(LocatorValue)).click();
		}
	}
	
	public static void Ok1(WebDriver driver, String locatorValue) throws Throwable
	{
		Thread.sleep(3000);
		
		List<WebElement> oks = driver.findElements(By.xpath(locatorValue));
	
		for(int i=0;i<oks.size();i++)
		{
			if(oks.get(i).getText().equalsIgnoreCase("OK!"))
			{
				oks.get(i).click();
				
				break;
			}
		}
	}
	public static void Search(WebDriver driver, String LocatorType, String LocatorValue, String Data) throws Exception
	{
		if(LocatorType.equalsIgnoreCase("id"))
		{
			Thread.sleep(3000);
			driver.findElement(By.id(LocatorValue)).sendKeys(Data);
			
		}
		else
			if(LocatorType.equalsIgnoreCase("name"))
		{
			Thread.sleep(3000);
			driver.findElement(By.name(LocatorValue)).sendKeys(Data);
		}
			else
				if(LocatorType.equalsIgnoreCase("xpath"))
		{
			Thread.sleep(3000);
			driver.findElement(By.xpath(LocatorValue)).sendKeys(Data);
		}
	}
	
	public static void UserValidation(WebDriver driver, String exp_Data) throws Throwable
	{

		if(driver.findElement(By.xpath(PropertyFileUtil.getValueForKey("Search.box"))).isDisplayed())
		{
			driver.findElement(By.xpath(PropertyFileUtil.getValueForKey("Search.box"))).clear();
		
			driver.findElement(By.xpath(PropertyFileUtil.getValueForKey("Search.box"))).sendKeys(exp_Data);
			
			driver.findElement(By.xpath(PropertyFileUtil.getValueForKey("Search.btn"))).click();
			
			Thread.sleep(2000);
		}
		
		
		WebElement webtable = driver.findElement(By.xpath(PropertyFileUtil.getValueForKey("Webtable.path")));
		
		List<WebElement> rows = driver.findElements(By.tagName("div"));
		

		for(int i=1;i<=rows.size();i++)
		{
			String act_data = driver.findElement(By.xpath("/html/body/app-root/app-authenticated/div/div/div[3]/users-list-root/div[2]/div/div[1]/div["+i+"]/div/div/div/h3/a")).getText();                             
//			Assert.assertEquals(act_data, exp_Data);
			if(act_data.contains(exp_Data))
				
			{
				Thread.sleep(3000);
				
				driver.findElement(By.xpath(PropertyFileUtil.getValueForKey("ViewUser.btn"))).click();
			}
			
			break;
		}
	}
	
	public static void focus (WebDriver driver)
	{
		WebElement elem = driver.switchTo().activeElement();
	}
	
// Selecting the dropdown
	public static void selectdropdown(WebDriver driver, String LocatorType, String LocatorValue, String Data) throws Exception
	{
		Select select = new Select(driver.findElement(By.xpath(LocatorValue)));
		select.selectByVisibleText(Data);	
	}
	
	
	public static void MultiselectClick(WebDriver driver) throws Throwable, Exception
	{
		Thread.sleep(2000);
    	// Click button
        driver.findElement(By.tagName("ss-multiselect-dropdown"))
                .findElement(By.tagName("button"))
                .click();
	}
        
        public static void MultiselectMembers(WebDriver driver,String LocatorValue,String LocatorType, String Data) throws Throwable, Exception
    	{
            	
            	Thread.sleep(2000);
            	
            // Click checkboxes
            driver.findElement(By.cssSelector("ss-multiselect-dropdown"))
                    .findElements(By.tagName("a"))
                    .stream()
                    .filter(elem -> 
                    {
                        String text = elem.getText();
                        System.out.println(text);
                        return text.contains(Data);
                    }).forEach(WebElement::click);
            }
            
            
            
//        public static void Site(WebDriver driver, String Data) throws Throwable{
//        	
//        	driver.findElement(By.xpath("/html/body/app-root/login/div[1]/div[2]/form/div[3]/select")).sendKeys(Keys.ENTER);
//        	Thread.sleep(2000);
//        	 WebElement campaignDeatils =  driver.findElement(By.xpath("/html/body/app-root/login/div[1]/div[2]/form/div[3]/select/option[contains(text(),"+Data+")]")); 
//        	 	Thread.sleep(3000);
//        	 	campaignDeatils.click();
//             		campaignDeatils.getText();
//             
        
       
	    
	    
	    
	
	//Select the Multiple groups option 
	

//Clicking on Add buttons
	public static void AddClick(WebDriver driver, String LocatorType, String LocatorValue, String Data) throws Throwable
	{
	  Thread.sleep(5000);
		  
		  driver.findElement(By.xpath(LocatorValue)).sendKeys(Keys.ENTER);	
	}
//ExplicitWait	
	public static void threadsleep(WebDriver driver) throws Throwable
	{
		Thread.sleep(3000);
	}
	
//Switch to the frames
	public static void switchtoframe(WebDriver driver, String LocatorType, String LocatorValue, String Data)
	{	
		driver.switchTo().frame(LocatorValue);
	}
	
//Wait for element
	public static void waitforelement(WebDriver driver, String LocatorType, String LocatorValue, String waittime)
	{
		WebDriverWait wait = new WebDriverWait(driver,Integer.parseInt(waittime));
		if(LocatorType.equalsIgnoreCase("id"))
		{
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(LocatorValue)));
		}
		else
			if(LocatorType.equalsIgnoreCase("name"))
			{
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(LocatorValue)));
			}
			else
				if(LocatorType.equalsIgnoreCase("xpath"))
				{
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(LocatorValue)));
				}
	}
	
	//Closing the browser
		public static void closebrowser(WebDriver driver)
		{
			driver.close();
		}

	
		

		

	
	
	
}
